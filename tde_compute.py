import pandas as pd
import os, sys, json, glob, pickle
from s3_fetch import s3_fetch
import subprocess
import pandas as pd
import json
from tde_setup import TDE_Setup
from tde_population_stats import Population_Report

class TDE_Compute:
	def __init__(self, config):
		self.config = config
		self.s3_obj = s3_fetch(config)
		self.tde_setup = TDE_Setup(config)
		self.tde_setup.setup()
		self.tde_setup.replicate()
		self.reporter = Population_Report(config)

	def make_jsons(self, df, output_path, filename):
		container = filename
		try:
			p = subprocess.Popen(f'mkdir {output_path}', shell=True)
			p.wait()
		except:
			print(f"Folder {output_path} already exists")
			pass

		df_final = df[["transaction_id", "ledger_entry", "amount", "date", "description"]]
		df_dict = df_final.to_dict('records')
		# print(len(df))
		count = 0
		num_of_trans = len(df_dict)
		if num_of_trans > 20000:
			for split_num in range(0, num_of_trans, 20000):
				final_file_name=f'./{output_path}/{filename+"_"+str(count)+".json"}'
				transaction_list = df_dict[split_num:split_num+20000]
				final_dict = {"container" : container, "cobrand_id" : 99, "use_cache": True, "debug" : True, "user_id" : 12177727, "transaction_list" : transaction_list}
				json.dump(final_dict,open(final_file_name, 'w'))
				count +=1
		else:
			final_file_name=f'./{output_path}/{filename+"_"+str(count)+".json"}'
			# print(final_file_name)
			transaction_list = df_dict
			final_dict = {"container" : container, "cobrand_id" : 99, "use_cache": True, "debug" : True, "user_id" :12177727, "transaction_list" : transaction_list}
			json.dump(final_dict,open(final_file_name, 'w'))

	def execute(self):
		self.s3_obj.download(self.config['source_file_path']['bucket'], 
							self.config['source_file_path']['file'], 
							self.config['source_file_path']['destination'])
		#print(self.config['source_file_path']['destination'])
		self.df = pd.read_excel(self.config['source_file_path']['destination'])
		df = self.df.fillna('')

		# Normalizing mandatory columns
		df.description = df.description.astype(str)
		df.container = df.container.apply(lambda x: x.lower())
		df.ledger_entry = df.ledger_entry.apply(lambda x: x.lower())

		# Adding dummy-values to other columns 
		df['transaction_id'] = df.index
		df['date'] = '2021-05-13'
		df['amount'] = 65.0

		# Creating DFs based on container
		df_bank = df[df['container'] == "bank"]
		df_card = df[df['container'] == "card"]
		df_loans = df[df['container'] == "loans"]

		self.make_jsons(df_bank, './input/', 'bank')
		self.make_jsons(df_card, './input/', 'card')
		self.make_jsons(df_loans, './input/', 'loans')

		_ = subprocess.Popen('mkdir ./output/', shell=True)
		_.wait()
        
		self.tde_setup.enrich()

		json_files = glob.glob(os.path.join('./output/','*.json'))

		output_transaction_list = []

		for file in json_files:
		    # print(file)
		    data = json.load(open(file, 'r'))
		    transaction_list = data['data']['transaction_list']
		    output_transaction_list.extend(transaction_list)
			
		# Creating output DF for output transaction list
		df_out = pd.DataFrame(output_transaction_list)

		# Merging input and output DF based on transaction_id
		df_final = pd.merge(df, df_out, on="transaction_id", how = "inner", suffixes=('', '_y'))

		df_final = df_final.fillna('')

		# Writing final DF to CSV if CSV file is required
		df_final.to_csv(self.config['joined_output_path'], index= False, header=True, sep = '|', escapechar="\\")

		output_file_name = self.reporter.run()

		p = subprocess.Popen("echo 'Uploading output files...'", shell=True)
		p.wait()
		self.s3_obj.upload('s3yodlee-dev', self.config['joined_output_path'], self.config['output_s3_path'] + self.config['joined_output_path'])
		self.s3_obj.upload('s3yodlee-dev', output_file_name, self.config['output_s3_path'] + output_file_name)


if __name__ == "__main__":
    session_config = json.load(open('sample_config.json','r'))
    tde_compute = TDE_Compute(session_config)
    tde_compute.execute()
