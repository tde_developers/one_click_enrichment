"""
Main file for the structure Initializing the whole pipeline

"""
from tde_compute import TDE_Compute
import json

if __name__ == '__main__':
    config = json.load(open('sample_config.json','r'))
    tde_pipeline = TDE_Compute(config)
    tde_pipeline.execute()

    
