from s3_fetch import s3_fetch
import subprocess
import time
import json
from os import listdir


class TDE_Setup:
	def __init__(self, config):
		self.s3_obj = s3_fetch(config)
		self.config = config

	def setup(self):
		# Tag Download
		bucket = self.config['tde_tag_path']['bucket']
		file = self.config['tde_tag_path']['file']
		destination = self.config['tde_tag_path']['destination']
		self.s3_obj.download(bucket, file, destination)
		self.s3_obj.unpack(self.config['tde_tag_path']['destination'],
		 					self.config['tde_tag_path']['destination'][:-4])

		# Model Download
		bucket = self.config['tde_model_path']['bucket']
		file = self.config['tde_model_path']['file']
		destination = self.config['tde_model_path']['destination']
		self.s3_obj.download(bucket, file, destination)
		self.s3_obj.unpack(self.config['tde_model_path']['destination'],
		 					self.config['tde_model_path']['destination'][:-4])

	def replicate(self):
		p = subprocess.Popen('chmod +x single_post.sh', shell=True)
		p.wait()
		# Make directories:
		for i in range(self.config['tde_instance']):
			p = subprocess.Popen(f'mkdir ./tde_{i}/', shell=True)
			p.wait()
			p = subprocess.Popen(f'mkdir ./tde_{i}/meerkat/', shell=True)
			p.wait()
			p = subprocess.Popen(f'mkdir ./tde_{i}/meerkat/merchant_category/', shell=True)
			p.wait()
			p = subprocess.Popen(f'mkdir ./tde_{i}/meerkat/merchant_category/models_lookup_data/', shell=True)
			p.wait()
			p = subprocess.Popen(f"cp -R {self.config['tde_tag_path']['destination'][:-4]}/* ./tde_{i}/", shell=True)
			p.wait()
			p = subprocess.Popen(f"cp -R ./models_lookup_data/* ./tde_{i}/meerkat/merchant_category/models_lookup_data/", shell=True)
			p.wait()

		# Start TDE servers
		for i, port in zip(range(self.config['tde_instance']),self.config['tde_ports']):
			_ = subprocess.Popen(f'cd ./tde_{i}/ && nohup /usr/bin/python3 -m meerkat.web_service --port_http {port-100} --port_https {port} --wipe_lru WIPE_LRU > nohup{i}.log 2>&1 &', shell=True)
		count = 5
		success = False
		while count:
			p = subprocess.Popen("echo 'Waiting for TDE instances to start...'", shell=True)
			p.wait()
			count -= 1
			resp = []
			for port in self.config['tde_ports']:
				p = subprocess.Popen(f"./single_post.sh one_ledger.json {port} > out.json", shell=True)
				p.wait()
				try:
					resp.append(json.load(open('out.json','r'))['status'])
				except Exception as e:
					print(e)                    
					resp.append('Fail')
			p = subprocess.Popen(f" echo {count} {resp}", shell=True)
			p.wait()
			if set(resp) == {'success'}:
				success = True
				break
			time.sleep(200)
		p = subprocess.Popen(f"echo 'TDE Launch: {success}'", shell=True)
		p.wait()
		return success
                    

	def enrich(self):
		l=listdir('./input/')
		l=list(filter(lambda x:x.endswith('.json'),l))

		done = list(filter(lambda x:x.endswith('.json'),listdir('./output/')))
		ports = self.config['tde_ports']
		pid = 0
		while len(listdir('./input'))!=len(listdir('./output/')):
			plist = []
			file_list = []
			for _ in listdir('./input/'):
				if _ not in listdir('./output/'):
					port = ports[pid]
					pid+=1
					p = subprocess.Popen(f"echo 'submitting on {port}'", shell=True)
					p.wait()
					p=subprocess.Popen('./single_post.sh '+'./input/'+_+' '+str(port)+' > ./output/'+_,shell=True)
					plist.append(p)
					file_list.append(_)
					if len(plist)==len(ports):
						pid=0
						break
			for _ in plist:
				_.wait()
			for _ in file_list:
				try:    
					temp = json.load(open('./output/' + _,'r'))
				except:
					flag = subprocess.Popen('rm ./output/'+_,shell=True)
					flag.wait()

if __name__ == "__main__":
	session_config = {"aws_access_key_id": "ASIASTYCK2HOXGNZMJI5",
					  "aws_secret_access_key": "3CKAu5rVRM2B3jtsh8B1bLBALfMthjSKWllw7N3x",
					  "aws_session_token": "FwoGZXIvYXdzELn//////////wEaDNy3HputieKebPj6VSKGAY5unZqk9pBcQCHSTPtCUhidhNXtWniw2D7kpsctG10yZyjJg5zvX1jyKCoRSsvKqTzOScHF13UCWJskDSzUWm853McoizteI695A3G9pUw0aLPZhvbp2BwD6i9TftnX/gOjN5zwRqR/D1UOCY+Pf8FJ1jcGcfJkoOlofmQ3u80W6S+IJDX5KJTxwpcGMijQcsLpI+onqeecXdb2arzAWr1riMZNEhyXzwNKogvLKSonJ3l3s1WU"}
	session_config['tde_tag_path'] = {"bucket": "s3yodlee-dev", "file": "aadib/gitrunner/tde/TDE-tde-clean-code-ygst-cacher-0096ee97c36f14096ad4f238aad85d21aae3db0b.tar", "destination": "./TDE-tde-clean-code-ygst-cacher-0096ee97c36f14096ad4f238aad85d21aae3db0b.tar"}
	session_config['tde_model_path'] = {"bucket": "s3yodlee-dev", "file": "aadib/gitrunner/tde/models_lookup_data.tar.gz", "destination": "./models_lookup_data.tar.gz"}
	session_config['tde_instance'] = 2
	session_config['tde_ports'] = [9000, 9001]
	tde = TDE_Setup(session_config)
	tde.setup()
	flag = tde.replicate()

