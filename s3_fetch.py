import boto3
import subprocess

class s3_fetch:
	"""
	This class fetches files from s3 and saves them into ec2.
	"""
	def __init__(self, session_config):
		self.s3_resource = boto3.resource('s3', region_name='us-west-2', aws_access_key_id=session_config['aws_access_key_id'], aws_secret_access_key=session_config['aws_secret_access_key'], aws_session_token=session_config['aws_session_token'])

	def download(self, bucket, file, destination):
		p = subprocess.Popen(f"echo '{bucket}, {file}, {destination}'", shell=True)
		p.wait()
		self.s3_resource.Object(bucket, file).download_file("./"+file.split('/')[-1])
        
	def upload(self, bucket, local_path, s3_path):
		self.s3_resource.Object(bucket, s3_path).upload_file(local_path)
		p = subprocess.Popen(f"echo 'Uploaded to bucket: {bucket}, path: {s3_path}'", shell=True)
		p.wait()

	def unpack(self, file_path, destination):
		destination = '.'
		p = subprocess.Popen(f'tar -xvf {file_path} -C {destination}', shell=True)
		p.wait()

"""
Test: Downloading a sample file

"""

if __name__ == "__main__":
	session_config = {"aws_access_key_id": "ASIASTYCK2HO262NKIK4",
    "aws_secret_access_key": "/QWG50zejl3pKrBak9emQDtJILyBTkKdd0cRSSHP",
    "aws_session_token": "FwoGZXIvYXdzEOj//////////wEaDOp6JEXxUyhe4g3t0iKGAd5AgVin0L1p3bZZBpzO1UfqpK09LYdzOrP7ib4+pmdFWVZsopm41f4tk7g0ph9C8T+edMZ2CsEoPsqma9/hyO16g08C6kIllDIr4SeEt/Fq6RIopIA90AXsh5T0vV7wdT+oHNakeZ1w3E3g+ktgJmZxmSxkI2m+U3d8hz8nFiodRKcSrV6UKNiZzZcGMihlZo26v7KBapCvooiBZQYHTc7Jq1XT75bAh9LUcGworioOeQiTootY"}
	s3_obj = s3_fetch(session_config)
	s3_obj.download('s3yodlee-dev','aadib/timer.json', './timer.json')
	s3_obj.upload('s3yodlee-dev', './timer.json', 'aadib/test_upload/timer.json')
