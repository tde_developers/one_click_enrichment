#usr/bin/env python
# coding: utf-8

import pandas as pd
import json
import xlsxwriter
import glob
import sys
import numpy as np

class Population_Report:
    def __init__(self, config):
        self.config = config
        self.region = config['region']
        self.list_of_features = config['list_of_features']
        self.file_path = config['validation_file']


    def output_stats(self):
        output_df=pd.read_csv(self.file_path, sep='|')
        print(output_df.head())	
        print(len(output_df))
        output_df=output_df.replace('', np.NaN)
        
        populated=[]
        ratio=[]
        un1=[]
        ratio1=[]
        for i in self.list_of_features:
            try:
                t=len(output_df)
                un= output_df[i].nunique()
                p=output_df[i].notnull().sum()
                r=str(round((p/t)*100))+ '%'
                r1=round((p/t)*100)
                populated.append(p)
                ratio.append(r)
                ratio1.append(r1)
                un1.append(un)
            except:
                populated.append(-1)
                ratio.append(0.0)
                ratio1.append(0.0)
                un1.append(1)
        return populated, ratio,ratio1, t, un1

    def run(self):
        file_name = self.config['validation_file']
        populated, ratio, ratio1,total_tx, un1 = self.output_stats()

        data1=['Features','Populated', 'Ratio','Unique_values']
        if '.csv' in file_name:
            file_name = file_name.split('.cs')[0]
        else:
            file_name = file_name.split('.xl')[0]
        root_dir = '.'
        path_delimiter = '/'
        workbook = xlsxwriter.Workbook(root_dir + path_delimiter + file_name + '_stats.xlsx')
        worksheet = workbook.add_worksheet()
        cell_format = workbook.add_format()
        cell_format1 = workbook.add_format()
        cell_format.set_bold()
        cell_format.set_bg_color('#CC99FF')
        cell_format1.set_indent(3)
        cell_format.set_indent(1)
        worksheet.write_row('A1', data1, cell_format)
         
        worksheet.write_column('A2', self.list_of_features) 
        worksheet.write_column('B2', populated, cell_format1) 
        worksheet.write_column('C2', ratio, cell_format1)
        worksheet.write_column('E2', ratio1, cell_format1)
        worksheet.write_column('D2', un1, cell_format1)
        worksheet.set_column('A:E', 15)
         
        
        chart2 = workbook.add_chart({'type': 'column'}) 

        chart2.add_series({
        'name':'=Population',
        'categories': '=Sheet1!$A$2:$A$80',
        'values':     '=Sheet1!$E$2:$E$80',
        'line':       {'color': 'red'},
        'data_labels': {'value':True},
        'marker': {'type': 'automatic'},
        'num_font':  {'bold': True}
        })
        title='TDE Output Stats('+str(total_tx)+' TRXN)'
        chart2.set_title({
            'name': title,
            'name_font': {
            'name': 'Calibri',
            'color': '#800080',
            'bold': True},
                      
        }) 
        chart2.set_x_axis({
            'name': 'Features',
            'name_font': {
            'name': 'Arial',
            'color': '#800080',
            'bold': True,
            'size': 15},
        })
        chart2.set_y_axis({
            'name': 'Populated Ratio',
            'name_font': {
            'name': 'Century',
            'color': '#800080',
            'bold': True,
            'size': 15},
        }) 
        
        chart2.set_style(11) 
        chart2.set_size({'width': 1500, 'height': 400})
        chart2.set_plotarea({
        'gradient': {'colors': ['#FFEFD1', '#F0EBD5', '#B69F66']}
    })
        worksheet.insert_chart('E2', chart2,{'x_offset': 25, 'y_offset': 10})
        workbook.close()
        return file_name + '_stats.xlsx'

if __name__ == "__main__": 
    """
    TODO: Need to write some tests here, take help from narendra.
    """
    
